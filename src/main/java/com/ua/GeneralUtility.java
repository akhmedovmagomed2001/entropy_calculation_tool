package com.ua;

import java.io.FileInputStream;
import java.io.IOException;

public class GeneralUtility {
    public static void checkArgs(String[] args) {

        boolean invalidLength = args.length != 2;
        boolean invalidSecondArg = !args[0].equals("-i");

        boolean isInvalid = invalidLength || invalidSecondArg;
        if (isInvalid) {
            String message = "It is required to specify input file: -i filepath";
            throw new RuntimeException(message);
        }
    }

    public static byte[] readFromFile(String filename) {
        byte[] result;

        try (FileInputStream fileInputStream = new FileInputStream(filename)) {
            result = new byte[fileInputStream.available()];
            fileInputStream.read(result);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return result;
    }
}
