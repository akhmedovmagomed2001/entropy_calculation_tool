package com.ua;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.ua.GeneralUtility.checkArgs;
import static com.ua.GeneralUtility.readFromFile;

public class EntropyCalculation {

    public static void main(String[] args) {
        checkArgs(args);

        String filename = args[1];
        byte[] inputBytes = readFromFile(filename);

        BigDecimal entropy = calculateEntropy(inputBytes);
        String entropyMessage = String.format("Entropy: '%s'", entropy);
        System.out.println(entropyMessage);

        int uniqueCharactersCount = calculateUniqueCharactersCount(inputBytes);
        String uniqueCharactersMessage = String.format("File contains %s unique characters", uniqueCharactersCount);
        System.out.println(uniqueCharactersMessage);

        BigDecimal maxEntropy = calculateMaxEntropy(inputBytes);
        String maxEntropyMessage = String.format("Max entropy: '%s'", maxEntropy);
        System.out.println(maxEntropyMessage);
    }

    private static BigDecimal calculateMaxEntropy(byte[] inputBytes) {
        int uniqueCharacters = calculateUniqueCharactersCount(inputBytes);
        byte[] uniqueBytes = new byte[uniqueCharacters];
        for (int i = 0, j = 0; i < uniqueCharacters; i++, j++) {
            uniqueBytes[j] = (byte) i;
        }
        return calculateEntropy(uniqueBytes);
    }

    private static BigDecimal calculateEntropy(byte[] inputBytes) {
        BigDecimal charactersCount = BigDecimal.valueOf(inputBytes.length, 8);

        return calculateCharacters(inputBytes).values()
                .stream()
                .map(count -> BigDecimal.valueOf(count, 8))
                .map(count -> count.divide(charactersCount, RoundingMode.HALF_UP))
                .map(probability -> {
                    BigDecimal logN = BigDecimal.valueOf(Math.log(probability.doubleValue()));
                    BigDecimal log2 = BigDecimal.valueOf(Math.log(2));
                    BigDecimal logProbability = logN.divide(log2, RoundingMode.HALF_UP);
                    return probability.multiply(logProbability);
                })
                .reduce(BigDecimal::add)
                .orElseThrow(RuntimeException::new)
                .multiply(BigDecimal.valueOf(-1))
                .setScale(3, RoundingMode.HALF_UP);
    }

    private static Map<Byte, Integer> calculateCharacters(byte[] inputBytes) {
        Map<Byte, Integer> characterCount = new HashMap<>();

        for (byte inputByte : inputBytes) {
            boolean containsKey = characterCount.containsKey(inputByte);

            if (containsKey) {
                Integer count = characterCount.get(inputByte);
                int newCount = count + 1;
                characterCount.put(inputByte, newCount);
            } else {
                characterCount.put(inputByte, 1);
            }
        }

        return characterCount;
    }

    private static int calculateUniqueCharactersCount(byte[] inputBytes) {
        Set<Byte> bytes = new HashSet<>();

        for (byte inputByte : inputBytes) {
            bytes.add(inputByte);
        }

        return bytes.size();
    }
}
