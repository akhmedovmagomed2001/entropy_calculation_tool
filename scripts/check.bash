cd ..
gradle clean jar
cd data
echo ""

echo "Check file data/AliceCH1.txt:"
java -jar ../build/libs/EntropyCalculationTool-1.0.jar -i AliceCH1.txt
echo ""

echo "Check file data/AES_AliceCH1.txt:"
java -jar ../build/libs/EntropyCalculationTool-1.0.jar -i AES_AliceCH1.hex
echo ""
